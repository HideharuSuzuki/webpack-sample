
const path = require('path');

module.exports = {
    // webpackの動作の種類(development or production or none)
	// development : エラー表示、デバッグしやすいファイルの出力、キャッシュ有効化(再ビルド時間短縮) => 開発時の出力
    // production  : ファイル圧縮、モジュールの最適化 => 本番ファイルの出力
    mode:'development',

    // エントリーポイントの設定
    // エントリーポイント：各モジュールを読み込んで、処理を開始するメインのファイル
    entry: './src/js/app.js',

    // 出力設定
    output:{
        // 出力ファイル名
        filename:'bundle.js',
        // 出力先のパス(絶対パス)
        path:path.resolve(__dirname, 'public/js')
    }
}


