
// export : 外部から利用できるようにした関数やオブジェクトの書き方
export default function add(number1, number2){
    return number1 + number2;
}